@extends('layouts.master')
@section('title', 'News Details')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Blog Details</h2>
                        <ul>
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>Blog Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Blog Details Area -->
    <section class="blog-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="blog-details-desc">
                        <div class="article-image">
                            <img src="{{asset($details->image)}}" alt="image">
                        </div>

                        <div class="article-content">
                            <div class="entry-meta">
                                <ul>
                                    <li>
                                        <span>Posted:</span>
                                        <a href="#">{{\Carbon\Carbon::parse($details->created_at)->diffForHumans()}}</a>
                                    </li>
                                    <li>
                                        <span>Posted By:</span>
                                        <a href="#">{{$details->user->name}}</a>
                                    </li>
                                </ul>
                            </div>
                            <h3>{{$details->title}}.</h3>
                            <p>
                                {{$details->message}}
                            </p>
                           </div>


                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Blog Details Area -->


@endsection
