@extends('layouts.master')
@section('title', 'Teaching Staff')
@section('content')
<!-- Start Page Banner -->
<div class="page-banner-area item-bg1">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="page-banner-content">
                    <h2>Staff</h2>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li>Teaching Staff</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Banner -->

<!-- Start Teacher Area -->
<section class="teacher-area pt-100 pb-70">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Glims Bond</h3>
                        <span>Music Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Sherlock Bin</h3>
                        <span>Art Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Priestly Herbart</h3>
                        <span>Math Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Smith Broke</h3>
                        <span>English Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>David Beckham</h3>
                        <span>Music Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Jacinda Meri</h3>
                        <span>Art Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Alex Maxwel</h3>
                        <span>Math Teacher</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-teacher">
                    <div class="image">
                        <img src="{{asset('assets/img/teacher/teacher-1.jpg')}}" alt="image">

                        <ul class="social">
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="content">
                        <h3>Carl Anderson</h3>
                        <span>English Teacher</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Teacher Area -->
    @endsection
