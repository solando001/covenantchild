@extends('layouts.master')
@section('title', 'Add Staff')
@section('content')

    <!-- Start Login Area -->
    <section class="apply-area ptb-100">
        <div class="container">
            <div class="apply-form">
                <h2>Add Staff</h2>

                @if(session('success'))
                    <div class="alert alert-success alert-block" id="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ session('success') }}
                    </div>
                @endif


                <form  action="{{url('post/staff')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
{{--                        <input id="email" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Title" required>--}}
                        <select name="title" class="form-control" required>
                            <option value=""> Select Title </option>
                            <option value="MR"> MR </option>
                            <option value="MRS"> MRS </option>
                            <option value="MISS"> MISS </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Full Name</label>
                            <input id="email" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Name" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label>Email Address</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input id="email" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone  Number" required>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>



                    <div class="form-group">
                        <label>Type</label>
                        {{--                        <input id="email" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Title" required>--}}
                        <select name="type" class="form-control" required>
                            <option value=""> Select Title </option>
                            <option value="teaching"> Teaching Staff </option>
                            <option value="non-teaching"> Non Teaching Staff </option>
                        </select>
                    </div>



                    <div class="form-group">
                        <label>Image</label><br>
                        <input type="file" name="image" required>
                    </div>


                    <button type="submit" class="default-btn">
                        Submit Now
                    </button>

                </form>

                {{--                <div class="important-text">--}}
                {{--                    <p>Don't have an account? <a href="register.html">Register now!</a></p>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Login Area -->
@endsection
