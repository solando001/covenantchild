@extends('layouts.master')
@section('title',  'COVENANT CHILD SCHOOLS')
@section('style')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 100%;
            /*margin: auto;*/
        }
        /*.container {width: 980px !important;}*/
    </style>
    @endsection
@section('content')

    <!-- Start Main Banner Area -->
    <div class="main-banner">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
{{--                <li data-target="#myCarousel" data-slide-to="2"></li>--}}
{{--                <li data-target="#myCarousel" data-slide-to="3"></li>--}}
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    <img src="{{asset('assets/img/who-we-are/who-we-are-3.jpg')}}" alt="Image1" width="460" height="345">
{{--                    <div class="carousel-caption">--}}
{{--                        <h3>Chania</h3>--}}
{{--                        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>--}}
{{--                    </div>--}}
                </div>

                <div class="item">
                    <img src="{{asset('assets/img/gallery/g6.jpg')}}" alt="Image2" width="460" height="345">
{{--                    <div class="carousel-caption">--}}
{{--                        <h3>Chania</h3>--}}
{{--                        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>--}}
{{--                    </div>--}}
                </div>

{{--                <div class="item">--}}
{{--                    <img src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg" alt="Flower" width="460" height="345">--}}
{{--                    <div class="carousel-caption">--}}
{{--                        <h3>Flowers</h3>--}}
{{--                        <p>Beatiful flowers in Kolymbari, Crete.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="item">--}}
{{--                    <img src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg" alt="Flower" width="460" height="345">--}}
{{--                    <div class="carousel-caption">--}}
{{--                        <h3>Flowers</h3>--}}
{{--                        <p>Beatiful flowers in Kolymbari, Crete.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>


{{--            <div class="main-banner-item banner-item-three">--}}
{{--            <div class="d-table">--}}
{{--                <div class="d-table-cell">--}}
{{--                    <div class="container">--}}
{{--                        <div class="main-banner-content">--}}
{{--                            <span>You are Great</span>--}}
{{--                            <h1>Please let's put a slider here. Its very important</h1>--}}
{{--                            <p>We have a mission to provide qualitative education in a Godly--}}
{{--                                and conducive environment serving to produce a child that will be--}}
{{--                                useful to God, himself and the society.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="main-banner-shape">--}}
{{--            <div class="banner-bg-shape">--}}
{{--                <img src="assets/img/main-banner/banner-bg-shape-1.png" alt="image">--}}
{{--            </div>--}}

{{--            <div class="banner-bg-shape-2">--}}
{{--                <img src="assets/img/main-banner/banner-bg-shape-2.png" alt="image">--}}
{{--            </div>--}}

{{--            <div class="banner-child">--}}
{{--                <div class="child-1">--}}
{{--                    <img src="assets/img/main-banner/child-1.png" alt="image">--}}
{{--                </div>--}}

{{--                <div class="child-2">--}}
{{--                    <img src="assets/img/main-banner/child-2.png" alt="image">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- End Main Banner Area -->

    <!-- Start Who We Are Area -->
    <section class="who-we-are ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="who-we-are-content">
                        <span>Who We Are</span>
                        <h3>Learn About Our Work and Cultural Activities</h3>
                        <p>We deliver balanced quality education while building on our
                            core values to develop a total child spiritually and morally to achieve sound character,
                            possibility, creativity, excellence and greatness! We provide outstanding facilities.</p>
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

                        <ul class="who-we-are-list">
                            <li>
                                <span>1</span>
                                Homelike Environment
                            </li>
                            <li>
                                <span>2</span>
                                Quality Educators
                            </li>
                            <li>
                                <span>3</span>
                                Computer and Sport Facilities
                            </li>
                            <li>
                                <span>4</span>
                                Play to Learn
                            </li>
                        </ul>
{{--                        <div class="who-we-are-btn">--}}
{{--                            <a href="#" class="default-btn">Read More</a>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="who-we-are-image-wrap">
                        <img src="assets/img/who-we-are/who-we-are-3.jpg" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Who We Are Area -->

    <!-- Start Fun Facts Area -->
    <section class="fun-facts-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="450">00</span>
                        </h3>
                        <p>Students</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-1">
                        <h3>
                            <span class="odometer" data-count="24">00</span>
                        </h3>
                        <p>Teachers</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-2">
                        <h3>
                            <span class="odometer" data-count="26">00</span>
                        </h3>
                        <p>Classrooms</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-3">
                        <h3>
                            <span class="odometer" data-count="2">00</span>
                        </h3>
                        <p>Buses</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Fun Facts Area -->

    <!-- Start Choose Area -->
    <section class="choose-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span>Why Choose Us</span>
                <h2>Our Core Values</h2>
            </div>

            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="single-choose">
                                <div class="icon">
                                    <i class='bx bx-bulb'></i>
                                </div>

                                <div class="content">
                                    <h3>Creative Activities</h3>
                                    <p>We train them to be creative from cradle. Learning is fun in
                                        our Pre-primary classes with colouring, drawing, bricks arrangement,
                                        toys play, palm printing, sand trace and cartoon.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="single-choose">
                                <div class="icon">
                                    <i class='bx bx-happy'></i>
                                </div>

                                <div class="content">
                                    <h3>Happy Environment</h3>
                                    <p>We provide a serene and learning-friendly environment</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="single-choose">
                                <div class="icon">
                                    <i class='bx bx-football'></i>
                                </div>

                                <div class="content">
                                    <h3>Amazing Playground</h3>
                                    <p>At CCS, we have amazing playground for our pre-primary and primary classes</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="single-choose">
                                <div class="icon">
                                    <i class='bx bx-book'></i>
                                </div>

                                <div class="content">
                                    <h3>Active Learning</h3>
                                    <p>Quality Education is our passion. We are always ready to raise child of great minds!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="choose-image">
                        <img src="assets/img/choose/choose.jpg" alt="image">

                        <div class="choose-image-shape">
                            <div class="shape-1">
                                <img src="assets/img/choose/choose-shape-1.png" alt="image">
                            </div>
                            <div class="shape-2">
                                <img src="assets/img/choose/choose-shape-2.png" alt="image">
                            </div>
                            <div class="shape-3">
                                <img src="assets/img/choose/choose-shape-3.png" alt="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Choose Area -->

    <!-- Start Class Area -->
{{--    <section class="class-area bg-fdf6ed pt-100 pb-70">--}}
{{--        <div class="container">--}}
{{--            <div class="section-title">--}}
{{--                <span>Classes</span>--}}
{{--                <h2>Popular Classes</h2>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('assets/img/gallery/g7.jpg')}}" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            <div class="price">$880</div>--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                <li>--}}
{{--                                    <span>Age:</span>--}}
{{--                                    3-5 Year--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Time:</span>--}}
{{--                                    8-10 AM--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            <div class="class-btn">--}}
{{--                                <a href="#" class="default-btn">Join Class</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('assets/img/gallery/g7.jpg')}}" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            --}}{{--                            <div class="price">$880</div>--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                <li>--}}
{{--                                    <span>Age:</span>--}}
{{--                                    3-5 Year--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Time:</span>--}}
{{--                                    8-10 AM--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            --}}{{--                            <div class="class-btn">--}}
{{--                            --}}{{--                                <a href="#" class="default-btn">Join Class</a>--}}
{{--                            --}}{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('assets/img/gallery/g7.jpg')}}" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            --}}{{--                            <div class="price">$880</div>--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                <li>--}}
{{--                                    <span>Age:</span>--}}
{{--                                    3-5 Year--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Time:</span>--}}
{{--                                    8-10 AM--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            --}}{{--                            <div class="class-btn">--}}
{{--                            --}}{{--                                <a href="#" class="default-btn">Join Class</a>--}}
{{--                            --}}{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="class-shape">--}}
{{--            <div class="shape-1">--}}
{{--                <img src="assets/img/class/class-shape-1.png" alt="image">--}}
{{--            </div>--}}
{{--            <div class="shape-2">--}}
{{--                <img src="assets/img/class/class-shape-2.png" alt="image">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Class Area -->

    <!-- Start Gallery Area -->
    <div class="gallery-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span>Gallery</span>
                <h2>Our Activities Gallery</h2>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g1.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g1.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g2.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g2.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g3.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g3.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g7.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g7.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g5.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g5.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g6.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g6.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Gallery Area -->

    <!-- Start Teacher Area -->
    <section class="teacher-area bg-ffffff pt-100 pb-70">
        <div class="container-fluid">
            <div class="section-title">
{{--                <span>Our Core Teacher</span>--}}
                <h2>Meet Our Directors</h2>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6"></div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-gallery-box">
                        <div class="images">
                            <img src="{{asset('CCProp.jpg')}}" alt="image">
                        </div>

                        <div class="content">
                            <h5>Pastor (Mrs) Oluseyi Aboyeji</h5>
                            <span>Proprietress</span>
                        </div>

                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-gallery-box">
                        <div class="images">
                            <img src="{{asset('assets/img/gallery/hos.jpg')}}" alt="image" height="400">
                        </div>
                        <div class="content">
                            <h5>Mrs Kehinde Sarumi</h5>
                            <span>DOS</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6"></div>

            </div>
        </div>
    </section>
    <!-- End Teacher Area -->

    <!-- Start Testimonials Area -->
    <section class="testimonials-area pt-100 pb-100">
        <div class="container">
            <div class="section-title">
                <span style="color: #000080">Testimonials</span>
                <h2>What Parents Say About Us</h2>
            </div>

            <div class="testimonials-slides owl-carousel owl-theme">
                <div class="testimonials-item">
                    <div class="testimonials-item-box">
                        <div class="icon">
                            <i class='bx bxs-quote-left'></i>
                        </div>
                        <p>My experience with the school is pretty good, my children have been in the school right from
                            Reception I. They have confidence anywhere they go. The school has impacted my children morally,
                            academically, socially and spiritually. I appreciate the rich learning environment and standard
                            teachers the school has.
                            All members, staffs of the school are really helpful, pupils receive everything they need at the school.
                        </p>
                        <div class="info-box">
                            <h5>Mrs. V.F Adeyemi</h5>
{{--                            <span>Music Teacher</span>--}}
                        </div>
                    </div>
                    {{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-1.png" alt="image">--}}
                    {{--                        </div>--}}
                </div>

                <div class="testimonials-item">
                    <div class="testimonials-item-box">
                        <div class="icon">
                            <i class='bx bxs-quote-left'></i>
                        </div>
                        <p>Covenant child school is very unique in all ramifications. It is a school with a taste when it comes to qualified teachers with quality delivery.

                            The school’s effectiveness and efficiency cannot be over emphasized, the management are well organized, the teachers have good relationship with both pupils and parents.
                            <br>
                            The teachers of CCS are very caring and lovely with modern teaching methods. Infact I like the whole school especially the location, environment and conducive classrooms.

                            CCS you are great!!!
                        </p>
                        <div class="info-box">
                            <h5>Mrs. Olanike Ogundare</h5>
{{--                            <span>Art Teacher</span>--}}
                        </div>
                    </div>
                    {{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-2.png" alt="image">--}}
                    {{--                        </div>--}}
                </div>

                <div class="testimonials-item">
                    <div class="testimonials-item-box">
                        <div class="icon">
                            <i class='bx bxs-quote-left'></i>
                        </div>
                        <p>
                            CCS is a school where every child loves to be. The pupils there are always eager to be in school and never want to go back home after school hours. Their qualified staff members, conducive environment and the best director of schools ever seen made the school unique!  They win any competition they go for.  CCS thank you for your great impacts upon our wards academically, and morally. You’re the best and I recommend you to the entire world. CCS, a total child! You’re all great.
                        </p>
                        <div class="info-box">
                            <h5>Mrs. Hassan</h5>
                            {{--                            <span>Music Teacher</span>--}}
                        </div>
                    </div>
                    {{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-1.png" alt="image">--}}
                    {{--                        </div>--}}
                </div>


                <div class="testimonials-item">
                    <div class="testimonials-item-box">
                        <div class="icon">
                            <i class='bx bxs-quote-left'></i>
                        </div>
                        <p>Covenant child school is just a place where dreams are fulfilled, a home away from home. A place where you ward can grow physically, mentally, academically and spiritually. A conducive atmosphere of peace, joy and harmony, a school with great taste, well qualified teachers and primitive words

                            I love the school
                            <br>
                            You are great.

                        </p>
                        <div class="info-box">
                            <h5>Mrs. A. O. Elizabeth</h5>
{{--                            <span>Math Teacher</span>--}}
                        </div>
                    </div>
                    {{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-3.png" alt="image">--}}
                    {{--                        </div>--}}
                </div>

                <div class="testimonials-item">
                    <div class="testimonials-item-box">
                        <div class="icon">
                            <i class='bx bxs-quote-left'></i>
                        </div>
                        <p>
                            CCS is a school where every child loves to be. The pupils there are always eager to be in school and never want to go back home after school hours. Their qualified staff members, conducive environment and the best director of schools ever seen made the school unique!  They win any competition they go for.  CCS thank you for your great impacts upon our wards academically, and morally. You’re the best and I recommend you to the entire world. CCS, a total child! You’re all great.
                        </p>
                        <div class="info-box">
                            <h5>Mrs. Hassan</h5>
{{--                            <span>Music Teacher</span>--}}
                        </div>
                    </div>
                    {{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-1.png" alt="image">--}}
                    {{--                        </div>--}}
                </div>

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            --}}{{--                            <h3>Sherlock Bin</h3>--}}
{{--                            <span>Head Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
                    {{--                            <img src="assets/img/testimonials/testimonials-2.png" alt="image">--}}
                    {{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Priestly Herbart</h3>--}}
{{--                            <span>Math Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-3.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Testimonials Area -->



    <!-- Start Quote Area -->
{{--    <section class="quote-area bg-fdf6ed pb-100">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="quote-image"></div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-6">--}}
{{--                    <div class="quote-item">--}}
{{--                        <div class="content">--}}
{{--                            <span>Get a Quote</span>--}}
{{--                            <h3>Online Class Registration</h3>--}}
{{--                        </div>--}}

{{--                        <form>--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="Your Name">--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="Email Address">--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="Studying Class">--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="Type Your Requirements">--}}
{{--                            </div>--}}

{{--                            <button type="submit" class="default-btn">--}}
{{--                                Submit Now--}}
{{--                            </button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Quote Area -->

    <!-- Start Blog Area -->
    <section class="blog-area bg-fdf6ed pb-70">
        <div class="container">
            <div class="section-title">
{{--                <span>News and Blog</span>--}}
                <h2>Latest News</h2>
            </div>

            <div class="row">
                @foreach($news as $new)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-item">
                        <div class="blog-image">
                            <a href="{{url('news/details/'.$new->slug)}}">
                                <img src="{{$new->image}}" alt="image">
                            </a>
                        </div>

                        <div class="blog-content">
                            <ul class="post-meta">
{{--                                <li>--}}
{{--                                    <span>By Admin:</span>--}}
{{--                                    <a href="#">{{$new->user->name}}</a>--}}
{{--                                </li>--}}
                                <li>
                                    <span>Created:</span>
                                    {{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}
                                </li>
                            </ul>
                            <h3>
                                <a href="{{url('news/details/'.$new->slug)}}">{{$new->title}}</a>
                            </h3>
                            <p>
{{--                                {{(substr($new->message, 0, 250)). '...'}}--}}
                                {!! substr(strip_tags($new->message) , 0, 200). '...' !!}
                            </p>

{{--                            <div class="blog-btn">--}}
{{--                                <a href="#" class="default-btn">Read More</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                    @endforeach


            </div>
        </div>
    </section>
    <!-- End Blog Area -->


@endsection
