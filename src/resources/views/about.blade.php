@extends('layouts.master')
@section('title', 'About Us')
@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>About</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>About</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Who We Are Area -->
    <section class="who-we-are ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="who-we-are-content">
                        <span>About</span>
                        <h3>Our Mission</h3>
                        <p>To provide qualitative education in a Godly and conducive environment, serving to produce a Child that will be useful to God, Himself and the Society.</p>
                        <ul class="who-we-are-list">
                            <li>
                                <span>1</span>
                                Homelike Environment
                            </li>
                            <li>
                                <span>2</span>
                                Quality Educators
                            </li>
                            <li>
                                <span>3</span>
                                Safety and Security
                            </li>
                            <li>
                                <span>4</span>
                                Play to Learn
                            </li>
                        </ul>
{{--                        <div class="who-we-are-btn">--}}
{{--                            <a href="#" class="default-btn">Read More</a>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="who-we-are-image-wrap">
                        <img src="{{asset('assets/img/gallery/abt.jpg')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Who We Are Area -->

    <!-- Start Fun Facts Area -->
    <section class="fun-facts-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="450">00</span>
                        </h3>
                        <p>Students</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-1">
                        <h3>
                            <span class="odometer" data-count="24">00</span>
                        </h3>
                        <p>Teachers</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-2">
                        <h3>
                            <span class="odometer" data-count="26">00</span>
                        </h3>
                        <p>Classrooms</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-3">
                        <h3>
                            <span class="odometer" data-count="2">00</span>
                        </h3>
                        <p>Buses</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Fun Facts Area -->



@endsection
