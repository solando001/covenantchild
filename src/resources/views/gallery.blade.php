@extends('layouts.master')
@section('title', 'Gallery')
@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Gallery</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Gallery Area -->
    <div class="gallery-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g1.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g1.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g2.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g2.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g3.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g3.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g7.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g7.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g5.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g5.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/img/gallery/g6.jpg')}}" alt="image">

                        <a href="{{asset('assets/img/gallery/g6.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>


            </div>
    </div>
    <!-- End Gallery Area -->


@endsection
