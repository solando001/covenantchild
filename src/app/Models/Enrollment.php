<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    use HasFactory;

    protected $table = "enrollments";

    protected $fillable = ['parent_name', 'parent_email', 'parent_phone', 'relationship', 'child_name', 'child_gender', 'child_age', 'child_class', 'address', 'is_admitted'];
}
